class ProjetosController < ApplicationController
	def index
		@products = Product.actives.paginate :per_page => 25, :order => "updated_at DESC", :page => params[:page]
	end

	def show
		unless Product.actives.exists?(params[:id])
			redirect_to projetos_path
			return
		end
		
		@product = Product.actives.find(params[:id])
		@comment = @product.comments.build
	end
	
	def buscar_custos
		@build_costs = BuildCost.all(:conditions => {:product_id => params[:product_id], :state => params[:state]})
		render :partial => "build_costs"
	end
	
	def salvar_comentario
	  unless Product.actives.exists?(params[:id])
			redirect_to projetos_path
			return
		end
		
		product = Product.actives.find(params[:id])
		comment = product.comments.build(params[:comment])
		if comment.save
		  flash[:notice] = 'Comentário criado com sucesso'
		else
		  flash[:error] = 'Erro ao criar o comentário, verifique se todas as informações foram preenchidas.'
	  end
		redirect_to projeto_path(product)
	end
end
