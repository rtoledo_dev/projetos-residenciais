class TaxFeesController < AdminController
  def new
    set_entity
  end
  
  def create
    set_entity
    @tax_fee.attributes = params[:tax_fee]
    if @tax_fee.save
      flash[:notice] = "Successfully created copy fee."
      redirect_to new_tax_fee_path
    else
      render :action => 'new'
    end
  end
  
  def edit
    set_entity
    render :action => 'new'
  end
  
  def update
    set_entity
    if @tax_fee.update_attributes(params[:tax_fee])
      flash[:notice] = "Successfully updated copy fee."
      redirect_to new_tax_fee_path
    else
      render :action => 'new'
    end
  end
  
  protected
  def set_entity
    @tax_fee = TaxFee.first
    @tax_fee ||= TaxFee.new
  end
end
