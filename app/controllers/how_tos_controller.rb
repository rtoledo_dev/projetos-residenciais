class HowTosController < AdminController
  def index
    @how_tos = HowTo.paginate :per_page => 10, :page => params[:page]
  end
  
  def show
    @how_to = HowTo.find(params[:id])
  end
  
  def new
    @how_to = HowTo.new
  end
  
  def create
    @how_to = HowTo.new(params[:how_to])
    if @how_to.save
      flash[:notice] = "Successfully created how to."
      redirect_to @how_to
    else
      render :action => 'new'
    end
  end
  
  def edit
    @how_to = HowTo.find(params[:id])
  end
  
  def update
    @how_to = HowTo.find(params[:id])
    if @how_to.update_attributes(params[:how_to])
      flash[:notice] = "Successfully updated how to."
      redirect_to @how_to
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @how_to = HowTo.find(params[:id])
    @how_to.destroy
    flash[:notice] = "Successfully destroyed how to."
    redirect_to how_tos_url
  end
end
