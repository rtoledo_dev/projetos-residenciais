class CopyFeesController < AdminController
  
  def new
    set_entity
  end
  
  def create
    set_entity
    @copy_fee.attributes = params[:copy_fee]
    if @copy_fee.save
      flash[:notice] = "Successfully created copy fee."
      redirect_to new_copy_fee_path
    else
      render :action => 'new'
    end
  end
  
  def edit
    set_entity
    render :action => 'new'
  end
  
  def update
    set_entity
    if @copy_fee.update_attributes(params[:copy_fee])
      flash[:notice] = "Successfully updated copy fee."
      redirect_to new_copy_fee_path
    else
      render :action => 'new'
    end
  end
  
  protected
  def set_entity
    @copy_fee = CopyFee.first
    @copy_fee ||= CopyFee.new
  end
  
end
