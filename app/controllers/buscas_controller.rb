class BuscasController < ApplicationController
  def index
    @search = Search.new params[:search]
    
    if request.post?
      @products = @search.find
    end
    
  end

end
