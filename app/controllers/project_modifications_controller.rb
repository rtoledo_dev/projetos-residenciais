class ProjectModificationsController < AdminController
  def index
    @project_modifications = ProjectModification.all(:order => "created_at desc")
  end
  
  def show
    @project_modification = ProjectModification.find(params[:id])
  end
  
  def destroy
    @project_modification = ProjectModification.find(params[:id])
    @project_modification.destroy
    flash[:notice] = "Successfully destroyed project modification."
    redirect_to project_modifications_url
  end
end
