class CommentsController < AdminController
  def index
    @comments = Comment.all(:conditions => {:comment_id => nil})
  end
  
  def publish
    @comment = Comment.find(params[:id])
    @comment.publish
    flash[:notice] = 'Comentário publicado com sucesso. Agora ele aparecerá na página do produto'
    redirect_to @comment
  end

  def show
    @comment = Comment.find(params[:id])
    if @comment.comment.nil?
      @comment_response = @comment.comments.build(:product_id => @comment.product_id)
    else
      @comment_response = @comment.comment
    end
  end

  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.find(params[:comment][:comment_id])
    @comment_response = @comment.comments.build(params[:comment])

    if @comment_response.save
      flash[:notice] = "Comentário criado com sucesso."
      redirect_to @comment
    else
      flash[:warning] = 'Erro ao responder comentário. Verifique a aba "Responder comentário"'
      render :action => 'show'
    end
  end

  def edit
    @comment = Comment.find(params[:comment][:comment_id])
    @comment_response = @comment.comment
  end

  def update
    @comment = Comment.find(params[:comment][:comment_id])
    @comment_response = @comment.comment
    if @comment_response.update_attributes(params[:comment])
      flash[:notice] = "Successfully updated comment."
      redirect_to @comment
    else
      render :action => 'show'
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    flash[:notice] = "Successfully destroyed comment."
    redirect_to comments_url
  end
end
