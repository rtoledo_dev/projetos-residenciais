class CartsController < AdminController
  before_filter :clear_empty
  
  def clear_empty
    carts = Cart.find(:all, :conditions => ["updated_at < ?",Time.now.last_month])
    carts.each{|t| t.destroy if t.cart_items.count <= 0}
  end
  
  def index
    @carts = Cart.paginate :per_page => 20, :page => params[:page]
  end
  
  
  def show
    @cart = Cart.find params[:id]
  end

  def update
    @cart = Cart.find params[:id]
    if @cart.update_attributes(params[:cart])
      flash[:notice] = "Produto atualizado com sucesso."
      redirect_to cart_path(:id => @cart.id)
    else
      redirect_to cart_path(:id => @cart.id)
    end
  end
  
  def destroy
    @cart = Cart.find(params[:id])
    @cart.destroy
    flash[:notice] = "Pedido removido com sucesso."
    redirect_to carts_url
  end

  def destroy_all
    carts = Cart.find params[:ids]
    carts.each{|t| t.destroy} if carts
    flash[:notice] = "Pedidos removidos com sucesso"
    redirect_to carts_url
  end
end
