class CarrinhoController < ApplicationController
  before_filter :set_cart
  
  def adicionar
    unless @cart.add_item(params[:product_value_ids].keys,params[:quantitys])
      flash[:warning] = "Não foi possível adicionar este item ao carrinho"
    end
    redirect_to :action => :index
  end

  def index

  end

  def finalizar
    session[:cart_id] = nil
    flash[:notice] = "Pedido concluído com sucesso"
    redirect_to root_path
  end

  def apagar
    cart_items = @cart.cart_items.find(:all, :conditions => {:product_id => params[:ids]})
    cart_items.each{|t| t.destroy}
    redirect_to :action => :index
  end

  def atualizar
    cart_item = @cart.update_item(params[:product_value_id],params[:quantity])
    render :text => cart_item ? cart_item.amount : 0
  end
  
  def pagamento
    @payment = @cart.payments.build
    @ps_order = PagSeguro::Order.new(@cart.id)

    # adicionando os produtos do pedido ao objeto do formulário
    @cart.cart_items.each_with_index do |cart_item,i|
      # Estes são os atributos necessários. Por padrão, peso (:weight) é definido para 0,
	  # quantidade é definido como 1 e frete (:shipping) é definido como 0.
	    values = {:id => cart_item.product.id, :price => cart_item.total_price, :quantity => cart_item.quantity, :description => cart_item.description}
	    values[:shipping] = TaxFee.first.price if i == 0

      @ps_order.add values
    end
  end
  
  def endereco
    @endereco = BuscaEndereco.por_cep(params[:id])#['Avenida', 'das Americas', 'Barra da Tijuca', 'RJ', 'Rio de Janeiro', '22640100']
    render :text => @endereco.to_json
  end
  
  def update_customer
    @cart.validate_customer_info = true
    if @cart.update_attributes(params[:cart])
      redirect_to :action => :index
    else
      render :action => :index
    end
  end
  
  def set_cart
    if !session[:cart_id] || !Cart.exists?(session[:cart_id])
      @cart = Cart.new 
      #@cart.user = current_user if current_user
      @cart.save
      session[:cart_id] = @cart.id
    else
      @cart = Cart.find(session[:cart_id])
    end
    
  end
  
  protected :set_cart

end
