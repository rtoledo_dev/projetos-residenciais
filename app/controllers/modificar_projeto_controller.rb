class ModificarProjetoController < ApplicationController
  def index
    
  end
  
  def novo
    @project_modification = ProjectModification.new
    if params[:tipo] == ProjectModification::PROJETO_EXISTENTE || params[:projeto]
      @project_modification.type_modification = ProjectModification::PROJETO_EXISTENTE
      @project_modification.product_id = Product.actives.find(params[:projeto]).id if Product.actives.exists?(params[:projeto])
    end
    @project_modification.type_modification ||= ProjectModification::PROJETO_CLIENTE
  end
  
  def criar
    @project_modification            = ProjectModification.new
    @project_modification.attributes = params[:project_modification]
    
    unless @project_modification.save
      render :action => :novo
    end
  end
  
  def buscar_projeto
    @project_modification = ProjectModification.new
    @project_modification.product_id = params[:id] unless params[:id].blank?
    render :partial => "product"
  end
  
  def endereco
    @endereco = BuscaEndereco.por_cep(params[:id])#['Avenida', 'das Americas', 'Barra da Tijuca', 'RJ', 'Rio de Janeiro', '22640100']
    render :text => @endereco.to_json
  end
  
  def map
    @project_modification = ProjectModification.new
    @project_modification.address = params[:address]
    @project_modification.neighborhood = params[:neighborhood]
    @project_modification.house_number = params[:house_number]
    @project_modification.city = params[:city]
    @project_modification.province = params[:province]
    @project_modification.set_initial_position
    puts @project_modification.attributes
    
    render :json => {:latitude => @project_modification.latitude.to_f, :longitude => @project_modification.longitude.to_f}
  end

end
