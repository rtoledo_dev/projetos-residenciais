class UsersController < AdminController

  def index
    @users = User.paginate :per_page => 10, :page => (params[:page] || 1), :order => "updated_at DESC"
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:notice] = "Usuário criado com sucesso"
      redirect_to users_url
    else
      flash.now[:error] = "Erro ao criar usuário"
      render :action => :new
    end
  end
  
  def edit
    @user = User.find(params[:id])
    render :edit
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      flash[:notice] = "Usuário atualizado com sucesso"
      redirect_to users_url
    else
      flash.now[:error] = "Erro ao atualizar usuário"
      render :action => :edit
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = "Successfully destroyed user."
    redirect_to users_url
  end
  
  def destroy_all
    users = User.find params[:ids]
    users.each{|t| t.destroy} if users
    flash[:notice] = "Usuários removidos com sucesso"
    redirect_to users_url
  end
  
end
