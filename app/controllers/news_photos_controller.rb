class NewsPhotosController < ApplicationController
  def index
    @news_photos = NewsPhoto.all
  end
  
  def show
    @news_photo = NewsPhoto.find(params[:id])
  end
  
  def new
    @news_photo = NewsPhoto.new
  end
  
  def create
    @news_photo = NewsPhoto.new(params[:news_photo])
    if @news_photo.save
      flash[:notice] = "Successfully created news photo."
      redirect_to @news_photo
    else
      render :action => 'new'
    end
  end
  
  def edit
    @news_photo = NewsPhoto.find(params[:id])
  end
  
  def update
    @news_photo = NewsPhoto.find(params[:id])
    if @news_photo.update_attributes(params[:news_photo])
      flash[:notice] = "Successfully updated news photo."
      redirect_to @news_photo
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @news_photo = NewsPhoto.find(params[:id])
    @news_photo.destroy
    flash[:notice] = "Successfully destroyed news photo."
    redirect_to news_photos_url
  end
end
