class IntegracaoController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def retorno
    session[:cart_id] = nil
    
    return unless request.post?
    # Se você receber pagamentos de contas diferentes, pode passar o
    # authenticity_token adequado como parâmetro para pagseguro_notification
    pagseguro_notification do |notification|
      Payment.create :status => notification.status, :payment_method => notification.payment_method, 
      :email => notification.buyer[:email], :name => notification.buyer[:name], :service_id => notification.transaction_id,
      :cart_id => notification.order_id
    end

    render :nothing => true
  end

end
