class Photo3dsController < AdminController
  
  def index
    @photo3ds = Photo3d.paginate :per_page => 10, :page => params[:page] || 1, :order => "product_id"
  end
  
  def show
    @photo3d = Photo3d.find(params[:id])
  end
  
  def new
    @photo3d = Photo3d.new    
  end
  
  def create
    @photo3d = Photo3d.new(params[:photo3d])
    if @photo3d.save
      flash[:notice] = "Successfully created photo3d."
      redirect_to photo3ds_path
    else
      render :index
    end
  end
  
  def edit
    @photo3d = Photo3d.find(params[:id])
  end
  
  def update
    @photo3d = Photo3d.find(params[:id])
    if @photo3d.update_attributes(params[:photo3d])
      flash[:notice] = "Successfully updated photo3d."
      redirect_to photo3ds_path
    else
      render :index
    end
  end
  
  def destroy
    @photo3d = Photo3d.find(params[:id])
    @photo3d.destroy
    flash[:notice] = "Successfully destroyed photo3d."
    redirect_to photo3ds_url
  end
  
 
end
