class ProductAttributesController < AdminController
  def index
    @product_attributes = ProductAttribute.admin_list params[:product_id],params[:page]
  end
  
  
  def new
    @product_attribute = ProductAttribute.new
  end
  
  def create
    @product_attribute = ProductAttribute.new(params[:product_attribute])
    if @product_attribute.save
      flash[:notice] = "Successfully created product attribute."
      redirect_to product_attributes_url
    else
      render :action => 'new'
    end
  end
  
  def edit
    @product_attribute = ProductAttribute.find(params[:id])
  end
  
  def update
    @product_attribute = ProductAttribute.find(params[:id])
    if @product_attribute.update_attributes(params[:product_attribute])
      flash[:notice] = "Successfully updated product attribute."
      redirect_to product_attributes_url
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @product_attribute = ProductAttribute.find(params[:id])
    @product_attribute.destroy
    flash[:notice] = "Successfully destroyed product attribute."
    redirect_to product_attributes_url
  end
end
