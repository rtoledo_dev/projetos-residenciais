class ProductValuesController < AdminController
  def index
    @product_values = ProductValue.admin_list params[:product_id],params[:page]
  end
  
  def new
    @product_value = ProductValue.new
  end
  
  def create
    @product_value = ProductValue.new(params[:product_value])
    if @product_value.save
      flash[:notice] = "Successfully created product value."
      redirect_to product_values_url
    else
      render :action => 'new'
    end
  end
  
  def edit
    @product_value = ProductValue.find(params[:id])
  end
  
  def update
    @product_value = ProductValue.find(params[:id])
    if @product_value.update_attributes(params[:product_value])
      flash[:notice] = "Successfully updated product value."
      redirect_to product_values_url
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @product_value = ProductValue.find(params[:id])
    @product_value.destroy
    flash[:notice] = "Successfully destroyed product value."
    redirect_to product_values_url
  end
end
