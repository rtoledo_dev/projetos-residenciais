class ProductsController < AdminController
  def index
    @products = Product.paginate :per_page => 10, :page => params[:page] || 1
  end
  
  def new
    @product = Product.new
    @product.set_dependencies
  end
  
  def create
    @product = Product.new(params[:product])
    if @product.save
      flash[:notice] = "Produto criado com sucesso."
      redirect_to products_path
    else
      render :action => :new
    end
  end
  
  def edit
    @product = Product.find(params[:id])
  end
  
  def update
    params[:product][:category_ids] ||= [] if params[:product]
    @product = Product.find(params[:id])
    if @product.update_attributes(params[:product])
      flash[:notice] = "Produto atualizado com sucesso."
      redirect_to products_path
    else
      render :action => :edit
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:notice] = "Produto removido com sucesso."
    redirect_to products_url
  end
  
end
