class HomeController < ApplicationController
  def index
    @products = Product.actives.find(:all, :limit => 10)
    @news = News.all :order => "id DESC"
  end

end
