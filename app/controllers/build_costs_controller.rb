class BuildCostsController < AdminController
  def index
    @build_costs = BuildCost.all
  end
  
  def show
    @build_cost = BuildCost.find(params[:id])
  end
  
  def new
    @build_cost = BuildCost.new
  end
  
  def create
    @build_cost = BuildCost.new(params[:build_cost])
    if @build_cost.save
      flash[:notice] = "Custo de obra criado."
      redirect_to @build_cost
    else
      render :action => 'new'
    end
  end
  
  def edit
    @build_cost = BuildCost.find(params[:id])
  end
  
  def update
    @build_cost = BuildCost.find(params[:id])
    if @build_cost.update_attributes(params[:build_cost])
      flash[:notice] = "Custo de obra atualizado com sucesso."
      redirect_to @build_cost
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @build_cost = BuildCost.find(params[:id])
    @build_cost.destroy
    flash[:notice] = "Custo de obra removido com sucesso."
    redirect_to build_costs_url
  end
end
