class ContatoController < ApplicationController
  def index
    @contact = Contact.new
  end

  def create
    @contact = Contact.new params[:contact]
    unless @contact.send_mail
      render :index
    end
  end

end
