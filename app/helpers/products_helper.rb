module ProductsHelper
  def products_filter
    options_for_select([nil]+Product.all.collect{|t| [t.name,t.id]},params[:product_id].to_i)
  end
end
