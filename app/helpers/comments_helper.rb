module CommentsHelper
  
  def opinions
    html = []
    html << ['Muito ruim',0]
    html << ['Ruim',1]
    html << ['Bom',2]
    html << ['Muito bom',3]
    html << ['Ótimo',4]
    html
  end
end
