# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  def bool_to_str(term)
    unless term
      "Não"
    else
      "Sim"
    end
  end

  def buttons(buttons_text = nil)
    return if buttons_text.nil?
    content_for(:buttons) do
      buttons_text
    end
    buttons_text
  end
  
  def title(title_text = nil)
    html = "Projetos Residenciais - "+title_text
    content_for(:title) do
      html
    end
    html
  end
  
  def pagination(collection)
    will_paginate collection, :previous_label => "« Voltar",:next_label => "Avançar »"
  end
  
  def breadcrumb
    return unless defined?(@breadcrumb)
    html = []
    @breadcrumb.each do |t|
      html << link_to(t[:text],t[:link])
    end
    html.join " > "
  end
  
  def current_class(location)
    if location.is_a? String
      "current" if location.gsub(/\//,"") == controller.controller_name
    else
      location.each do |l|
        return "current" if l.gsub(/\//,"") == controller.controller_name
      end
    end
  end
  
  def error_notification
    html = ""
    flash.each do |name, msg|
      html << content_tag(:div, 
        ('<a href="javascript:;" class="close"><img src="/images/icons/cross_grey_small.png" title="Close this notification" alt="close"></a>' +
        content_tag(:div, msg)), 
        :id => "flash_#{name}", :class => "notification #{name} png_bg")
    end
    
    html
  end
  
  def edit_icon(url)
    link_to image_tag("icons/pencil.png"), url, :title => "Editar"
  end

  def delete_icon(url)
    link_to image_tag("icons/cross.png"), url,  :method => :delete, :title => "Excluir", :confirm => "Tem certeza disto?"
  end
  
   def class_if_current_page(location,add_class = nil)
     active = location.include?(controller.controller_name) ? "active" : nil
     classes = [active,add_class].compact
     if classes.size > 0
       "class=\"#{classes.join(' ')}\""
     end
     
   end
   
   def link_to_seo(text,url,html_options = {})
     html_options = {:title => text}.merge(html_options)
     link_to text,url, html_options
   end
   
   def link_to_project(text,url)
     link_to_seo(text,url,:title => project_title(text))
   end
   
   
   def project_title(title)
     "Projeto - #{title}"
   end
   
   def image_tag_seo(url,html_options = {})
     html_options = {:title => "Projetos residenciais",:alt => "Projetos residenciais"}.merge(html_options)
     image_tag url, html_options
   end
   
   def states
     ["AC",
     "AL",
     "AP",
     "AM",
     "BA",
     "CE",
     "DF",
     "ES",
     "GO",
     "MA",
     "MS",
     "MT",
     "MG",
     "PA",
     "PB",
     "PR",
     "PE",
     "PI",
     "RJ",
     "RN",
     "RS",
     "RO",
     "RR",
     "SC",
     "SP",
     "SE",
     "TO"]
   end
   
   def remove_link_unless_new_record(fields)
     out = ''
     out << fields.hidden_field(:_delete)  unless fields.object.new_record?
     out << link_to("remove", "##{fields.object.class.name.underscore}", :class => 'remove')
     out
   end

   # This method demonstrates the use of the :child_index option to render a
   # form partial for, for instance, client side addition of new nested
   # records.
   #
   # This specific example creates a link which uses javascript to add a new
   # form partial to the DOM.
   #
   #   <% form_for @project do |project_form| -%>
   #     <div id="tasks">
   #       <% project_form.fields_for :tasks do |task_form| %>
   #         <%= render :partial => 'task', :locals => { :f => task_form } %>
   #       <% end %>
   #     </div>
   #   <% end -%>
   def generate_html(form_builder, method, options = {})
     options[:object] ||= form_builder.object.class.reflect_on_association(method).klass.new
     options[:partial] ||= method.to_s.singularize
     options[:form_builder_local] ||= :f  

     form_builder.fields_for(method, options[:object], :child_index => 'NEW_RECORD') do |f|
       render(:partial => options[:partial], :locals => { options[:form_builder_local] => f })
     end
   end

   def generate_template(form_builder, method, options = {})
     escape_javascript generate_html(form_builder, method, options)
   end
end
