module ProjetosHelper
  def category_names(product)
    product.categories.collect{|t| link_to t.name, categoria_path(t.public_id)}.join(", ")
  end

  def link_add_product(product_value_id,quantity = 1)
    url_for(:controller => :carrinho, :action => :adicionar, :product_value_id => product_value_id)
  end

  def criar_projeto?
    controller.action_name == "novo_projeto" || controller.action_name == "criar_projeto"
  end
  
  def orientation_options
    "A frente do terreno,Atrás do terreno,Ao lado direito do terreno,Ao lado esquerdo do terreno".split(",")
  end
  
  def climatic_options
    "Quente,Frio,Seco,Árido,Úmido".split(",")
  end
  
  def yes_no_options
    [[nil,nil],["Sim",true],["Não",false]]
  end
end
