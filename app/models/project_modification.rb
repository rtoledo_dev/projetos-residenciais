include Geokit::Geocoders
class ProjectModification < ActiveRecord::Base
  PROJETO_EXISTENTE = "projeto-existente"
  PROJETO_CLIENTE   = "projeto-do-cliente"
  PROJETO_A_CRIAR   = "projeto-a-criar"
  belongs_to :product
  
  validates_presence_of :name, :email, :phone, :address, :city, :province, 
  :cep, :right_side, :left_side, :front_side, :back_side, :orientation, 
  :climatic, :type_modification, :neighborhood,:house_number,
  :customer_cep, :customer_address_number, :customer_address_neighborhood, 
  :customer_address, :customer_address_city, 
  :customer_address_state
  validates_presence_of :product_id, :if => :to_project
  
  validates_numericality_of [:right_side, :front_side, :left_side, :back_side,
  :quantity_people, :quantity_cars], :allow_nil => true
  
  has_attached_file :localization
  has_attached_file :picture
    
  validates_attachment_content_type :picture, 
  :content_type => ["image/jpeg", "image/png", "image/gif", 
    "image/pjpeg", "image/x-png","image/png","image/jpg"],
  :message => I18n.t("activerecord.errors.messages.invalid"), 
  :allow_nil => true
  
  before_validation :set_type_modification
  before_save :set_initial_position

  def to_project
    self.type_modification == PROJETO_EXISTENTE
  end

  
  def set_type_modification
    return unless self.type_modification.nil?
    self.type_modification = self.product_id ? PROJETO_EXISTENTE : PROJETO_CLIENTE
  end
  
  def full_address
    [
      "#{self.address}, #{self.house_number}",
      self.city,
      "#{self.province}, #{self.cep}"
    ].join(' - ')
  end
  
  
  def set_initial_position
    begin
      res = MultiGeocoder.geocode(self.full_address)
      unless res.success
        address = "#{self.city} - #{self.province}, #{self.cep}"
        res     = MultiGeocoder.geocode(address)
      end
      self.latitude, self.longitude = res.ll.split(",")
    end
  end
  
  def update_position
    self.set_initial_position
    self.update_attribute :latitude, self.latitude unless self.latitude.nil?
    self.update_attribute :longitude, self.longitude unless self.latitude.nil?
  end
  
end
