class Profile < ActiveRecord::Base
  has_and_belongs_to_many :roles
  validates_presence_of :description
  validates_uniqueness_of :description

  validate :validate_roles
  
  def validate_roles
    if roles.size == 0 && role_ids.size == 0
      self.errors.add(:role_ids, "Deve ter ao menos uma regra")
    end
  end
end
