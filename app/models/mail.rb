class Mail < ActionMailer::Base
  def contact(name,email,subject,message)
    subject    'Contato pelo site'
    recipients 'niltonrezende@hotmail.com'
    from       'faleconosco@projetosresidenciais.com.br'
    sent_on    Time.now
    
    body       :name => name, :email => email, :subject => subject, :message => message
  end
  
  
  def comment_notification(product_name,name,email,message)
    subject    "Comentário do projeto - #{product_name}"
    recipients 'niltonrezende@hotmail.com'
    from       'faleconosco@projetosresidenciais.com.br'
    sent_on    Time.now
    
    body       :name => name, :email => email, :message => message, :product_name => product_name
  end
end
