class User < ActiveRecord::Base
  acts_as_authentic
  acts_as_authorization_subject
  
  usar_como_cpf :cpf
  
  validates_presence_of :name, :email, :cpf
  validates_uniqueness_of :email
  validates_uniqueness_of :login
end
