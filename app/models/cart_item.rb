class CartItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :product
  belongs_to :product_value
  
  validates_presence_of :price, :quantity, :product_value_id, :product_id, :cart_id
  validates_associated :product
  validates_associated :product_value
  validates_numericality_of :quantity, :greater_than => 0
  
  validate :validate_product_value
  before_validation :set_price
  
  def description
    if number_copys > 0
      [product.name,product_value.product_type.name].join(", ") + " + #{number_copys} cópia(s)"
    else
      [product.name,product_value.product_type.name].join(", ")
    end
  end
  
  def total_price
    (number_copys * CopyFee.first.price) + price
  end
  
  protected 

  def validate_product_value
    if product
      unless product.product_values.exists?(product_value_id)
        erros.add :product_value_id, "O valor de produto não existe"
      end
    end
  end
  
  def set_price
    self.price = product_value.description.to_f
    self.amount = total_price * quantity
  end
end
