class Photo < ActiveRecord::Base
  has_attached_file :photo, :styles => { :webdoor => "230x230#", :thumb => "130x130#", :list => "50x50#", :big => "900>", :normal => "300" }, :default_style => :webdoor
  belongs_to :product
  validates_presence_of :product_id
  
  validates_attachment_presence :photo
  
  def image
    photo.url(:webdoor)
  end
end
