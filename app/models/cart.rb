class Cart < ActiveRecord::Base
  default_scope :order => "updated_at DESC"
  
  belongs_to :user
  has_many :cart_items, :dependent => :destroy
  has_many :payments, :dependent => :destroy
  has_one  :payment

  attr_accessor :validate_customer_info
  validates_presence_of :customer, :email, :phone, :phone_prefix, 
  :customer_cep, :customer_address_number, :customer_address_neighborhood, 
  :customer_address, :customer_address_city, 
  :customer_address_state, :if => :validate_customer_info?

  def validate_customer_info?
    self.validate_customer_info
  end
  
  def incomplete
    [
      :customer, :email, :phone, :phone_prefix, :customer_cep, :customer_address, 
      :customer_address_number, :customer_address_neighborhood, 
      :customer_address_city, :customer_address_state
    ].each do |_attr|
      return true if self.read_attribute(_attr).blank?
    end
    return false
  end
  
  def amount
    cart_items.sum(:amount) + TaxFee.first.price
  end
  
  attr_accessor :products_
  def products
    return self.products_ unless self.products_.nil?
    
    products = []
    cart_items.each do |t|
      products << t.product_id
    end
    
    self.products_ = Product.find(:all, :conditions => {:id => products.uniq})
  end
  
  attr_accessor :itens_per_product_
  def itens_per_product(product)
    self.itens_per_product_ ||= {}
    return self.itens_per_product_[product.id] unless self.itens_per_product_[product.id].nil?
    
    self.itens_per_product_[product.id] = cart_items.find(:all, :conditions => ['cart_items.product_id = ?', product.id])
  end
  
  
  def quantity
    cart_items.sum(:quantity)
  end
  
  def price_per_product(product)
    return 0 if product.nil?
    cart_items.sum(:amount, :conditions => {:product_id => product.id})
  end
  
  # tenta adicionar um item ao carrinho atual
  # caso o produto ou o valor do mesmo não exista retorna uma excessão
  def add_item(product_value_ids,quantity_copys)
    quantitys ||= {}
    return false if product_value_ids.nil? || product_value_ids.size == 0
    
    product_values = ProductValue.find(product_value_ids)
    
    CartItem.transaction do
      cart_items.find(:all, :conditions => ['product_id = ?', product_values.first.product_id]).each{|t| t.destroy}


      product_values.each do |product_value|
        cart_item_attributes = {:product_id => product_value.product_id, :product_value_id => product_value.id}

        cart_items_attributes = {:product_id => product_value.product_id, :product_value_id => product_value.id}
        cart_item = cart_items.build cart_item_attributes
        cart_item.quantity = 1
        cart_item.number_copys = quantity_copys[product_value.id.to_s] if quantity_copys.has_key?(product_value.id.to_s)
        cart_item.save!
      end
      true
    end
  rescue
    false
  end
  
  # tenta atualizar um item do carrinho atual
  # caso o produto ou o valor do mesmo não exista, retorna uma excessão
  # caso não exista o item que está tentando adicionar, o item é criado chamando a função 'add_item'
  def update_item(product_value_id,new_quantity)
    product_value = ProductValue.find(product_value_id)
    
    cart_item = cart_items.first(:conditions => {:product_id => product_value.product_id, :product_value_id => product_value.id})
    
    unless cart_item
      return add_item(product_value.product_id,product_value.id)
    end
    
    cart_item.quantity = new_quantity
    
    unless cart_item.valid?
      return false
    end
    
    cart_item.save
    cart_item
  end
  
end
