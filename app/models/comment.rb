class Comment < ActiveRecord::Base
  
  default_scope :order => "active"
  attr_protected :active
  belongs_to :product
  belongs_to :answer, :class_name => "Comment", :foreign_key => :comment_id
  has_one    :comment, :dependent => :destroy
  has_many   :comments, :dependent => :destroy
  validates_presence_of :name, :message, :product_id, :email
  
  after_create :send_email
  
  before_create :active_comment
  
  def active_comment
    if self.comment_id
      self.active = true
    end
  end
  
  def send_email
    unless self.comment_id
      Mail.deliver_comment_notification(self.product.name,self.name,self.email,self.message)
    end
    true
  end
  
  def publish
    self.active = true
    self.save
  end
end
