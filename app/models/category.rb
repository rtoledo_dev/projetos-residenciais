class Category < ActiveRecord::Base
  validates_presence_of :name
  validates_uniqueness_of :name
  
  has_and_belongs_to_many :products
  
  default_scope :order => "name"
  
  named_scope :actives
  
  def public_id
    [id].concat(name.split(" ")).collect{|t| t.to_s.downcase.strip}.compact.join("-").gsub(/[,|.|\/|?]/,'')
  end
end
