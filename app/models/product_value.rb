class ProductValue < ActiveRecord::Base
  belongs_to :product
  belongs_to :product_type
  
  named_scope :comum, :conditions => {:product_types => {:name => "Comum"}}, :joins => :product_type
  
  def to_s
    description
  end
  
  def self.admin_list(product_id,page)
    page ||= 1
    if product_id.blank?
      ProductValue.paginate :per_page => 20, :page => page, :order => "product_id,product_type_id"
    else
      ProductValue.paginate :per_page => 100, :page => page, :order => "product_id,product_type_id", :conditions => {:product_id => product_id}
    end
  end
end
