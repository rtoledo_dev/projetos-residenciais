class TaxFee < ActiveRecord::Base
  validates_presence_of :price
  validates_numericality_of :price
  validate :uniq_value
  
  def uniq_value
    if self.new_record? && TaxFee.count > 0
      self.errors.add :price, "Você só pode adicionar um valor para a taxa de correios"
    end
  end
end
