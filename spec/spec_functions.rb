def spec_current_user 
  @current_user ||= User.find(users(:administrator).id)
end 

# returns current session mock 
def spec_user_session(current_user_obj = nil)
  current_user_obj ||= spec_current_user
  @user_session = mock 
  @user_session.stubs(:user).returns(current_user_obj) 
  @user_session.stubs(:record).returns(current_user_obj)
  @user_session 
end 

def spec_login(user_session_obj = nil)
  user_session_obj ||= spec_user_session
  
  UserSession.stubs(:find).returns( user_session_obj ) 
end

def spec_login_for_role(role)
  user = User.new
  user.stubs(:valid?).returns(true)
  user.has_role!(role)
  spec_login(spec_user_session(user))
end


def attributes_for_valid_user
  {
    :login => "xxx",
    :name => "xxx", 
    :password => "123456",
    :password_confirmation => "123456",
    :cpf => "812.255.928-01",
    :email => "email@xxx.com.br"
  }
end