require 'spec_helper'

describe User do
  fixtures :users
  
  before(:each) do
    @valid_attributes = attributes_for_valid_user
  end
  
  it "should not create a user with invalid attributes" do
    u = User.new
    u.save.should be_false
  end
  
  it "should not create a user with invalid cpf" do
    u = User.new @valid_attributes
    u.cpf = "111"
    u.save.should be_false
    u.errors.on(:cpf).should_not be_nil
  end
  
  it "should have a Admin role" do
    u = User.find(users(:administrator).id)
    u.has_role?("Admin").should
  end
  
  it "should add a New role" do
    u = User.last
    u.has_role?("NewRole").should_not be_true
    u.has_role!("NewRole").should
  end
end