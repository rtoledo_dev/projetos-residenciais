require File.dirname(__FILE__) + '/../spec_helper'

describe CopyFee do
  fixtures :copy_fees
  
  it "should not be valid" do
    CopyFee.new.should_not be_valid
  end
  
  it "should not be valid because there is only one register" do
    c = CopyFee.new :price => 20
    c.save.should be_false
  end
  
  it "should be valid because there is only one register" do
    CopyFee.destroy_all
    c = CopyFee.new :price => 20
    c.save.should
  end
end
