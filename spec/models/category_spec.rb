require 'spec_helper'

describe Category do
  fixtures :all
  
  before(:each) do
    @valid_attributes = {:name => "Suite"}
  end
  
  it "should be valid" do
    c = Category.new @valid_attributes
    c.save.should be_true
  end
  
  it "should be invalid with blank name" do
    c = Category.new
    c.save.should be_false
    c.errors.on(:name).should_not be_nil
  end
  
  it "should be invalid with duplicate name" do
    c = Category.new :name => "2 quartos"
    c.save.should be_false
    c.errors.on(:name).should_not be_nil
  end
end
