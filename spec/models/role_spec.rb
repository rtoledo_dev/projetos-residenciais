require 'spec_helper'

describe Role do
  fixtures :roles, :users, :roles_users
  
  before(:each) do
    @valid_attributes = {
      :name => "all_role"
    }
  end

  it "should create a new instance given valid attributes" do
    role = Role.new @valid_attributes
    role.save.should
  end
  
  it "should have only 2 roles" do
    Role.count.should eql(2)
  end
  
  it "should have 2 users in role comprar" do
    Role.find(roles(:comprar)).users.count.should eql(2)
  end
end
