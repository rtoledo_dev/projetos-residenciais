require 'spec_helper'

describe Product do
  fixtures :all
  
  before(:each) do
    @valid_attributes = {:name => "Projeto 2 quartos",:price => 900,:is_active => true,
      :resume => "Projeto de 2 quartos...", :description => "Projeto de 2 quartos...",
      :category_ids => [categories("2_quartos").id]}
  end
  
  it "should be valid" do
    p = Product.new @valid_attributes
    p.save.should
  end
  
  it "should be invalid with blank name" do
    p = Product.new @valid_attributes
    p.name = nil
    p.save.should be_false
    p.errors.on(:name).should_not be_nil
  end
  
  it "should be invalid with duplicate name" do
    p = Product.new @valid_attributes
    p.set_dependencies
    p.name = nil
    p.save.should be_false
    p.errors.on(:name).should_not be_nil
  end
  
  it "should have all categories" do
    p = Product.find(products(:sobrado).id)
    p.categories.count.should eql(6)
  end
end
