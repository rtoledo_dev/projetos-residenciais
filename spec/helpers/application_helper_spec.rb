require 'spec_helper'
include ApplicationHelper
require "will_paginate"

describe ApplicationHelper do
  fixtures :users
  
  def will_paginate(*args)
    return if args.nil? || args.first.nil?
    "pagination"
  end
  
  it "should have one button in content buttons" do
    buttons("Novo").should_not be_nil
  end
  
  it "should have title" do
    title("Edição").should eql("<h2>Edição</h2>")
  end
  
  it "should have a empty breadcrumb" do
    breadcrumb.should be_nil
  end
  
  it "should have a breadcrumb with two itens" do
    @breadcrumb = []
    @breadcrumb << {:text => "Home", :link => root_path}
    @breadcrumb << {:text => "Users", :link => users_path}
    breadcrumb.should_not be_nil
  end
  
  it "should print a pagination" do
    users = User.paginate :per_page => 1, :page => 1
    pagination(users).should_not be_nil
  end
  
  it "should not print a pagination with nil collection" do
    users = nil
    pagination(users).should be_nil
  end
  
  
  
end
