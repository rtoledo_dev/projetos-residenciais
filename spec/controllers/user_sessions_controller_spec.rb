require 'spec_helper'
 
describe UserSessionsController do
  fixtures :all
  
  def valid_attributes
    {:user_session => {
      :login => "rodrigo.toledo",
      :password => "raposa"
      }
    }
    
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    post :create, valid_attributes
    response.should redirect_to(admin_root_url)
  end
  
  it "destroy action should destroy model and redirect to index action" do
    @user_session = spec_user_session
    @user_session.stubs(:destroy).returns(true)
    spec_login(@user_session)
    get :destroy
    
    response.should redirect_to(root_url)
  end
  
  it "should get denied action" do
    get :denied
  end
end
