require 'spec_helper'
 
describe AttributesController do
  fixtures :all
  
  before(:each) do
    spec_login
  end
  
  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    Attribute.any_instance.stubs(:valid?).returns(false)
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    Attribute.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(attributes_url)
  end
  
  it "edit action should render edit template" do
    get :edit, :id => Attribute.first
    response.should render_template(:edit)
  end
  
  it "update action should render edit template when model is invalid" do
    Attribute.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Attribute.first
    response.should render_template(:edit)
  end
  
  it "update action should redirect when model is valid" do
    Attribute.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Attribute.first
    response.should redirect_to(attributes_url)
  end
  
  it "destroy action should destroy model and redirect to index action" do
    attribute = Attribute.first
    delete :destroy, :id => attribute
    response.should redirect_to(attributes_url)
    Attribute.exists?(attribute.id).should be_false
  end
end
