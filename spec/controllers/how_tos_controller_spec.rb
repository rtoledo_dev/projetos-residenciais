require File.dirname(__FILE__) + '/../spec_helper'
 
describe HowTosController do
  fixtures :all
  integrate_views
  
  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end
  
  it "show action should render show template" do
    get :show, :id => HowTo.first
    response.should render_template(:show)
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    HowTo.any_instance.stubs(:valid?).returns(false)
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    HowTo.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(how_to_url(assigns[:how_to]))
  end
  
  it "edit action should render edit template" do
    get :edit, :id => HowTo.first
    response.should render_template(:edit)
  end
  
  it "update action should render edit template when model is invalid" do
    HowTo.any_instance.stubs(:valid?).returns(false)
    put :update, :id => HowTo.first
    response.should render_template(:edit)
  end
  
  it "update action should redirect when model is valid" do
    HowTo.any_instance.stubs(:valid?).returns(true)
    put :update, :id => HowTo.first
    response.should redirect_to(how_to_url(assigns[:how_to]))
  end
  
  it "destroy action should destroy model and redirect to index action" do
    how_to = HowTo.first
    delete :destroy, :id => how_to
    response.should redirect_to(how_tos_url)
    HowTo.exists?(how_to.id).should be_false
  end
end
