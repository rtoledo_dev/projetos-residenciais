require 'spec_helper'

describe ApplicationController do
  fixtures :users

  #Delete these examples and add some real ones
  it "should use ApplicationController" do
    controller.should be_an_instance_of(ApplicationController)
  end
  
  it "should have a user in session" do
    spec_login
    controller.current_user_session.should_not be_nil
  end
  
  it "should have a current user" do
    spec_login
    controller.current_user.should_not be_nil
  end
  
  it "should not permit access to user" do
    Acl9::AccessDenied.stubs(:new)
    spec_login
    get :access_denied
    response.should redirect_to(denied_path)
  end
  
  it "should not permit access to anonymous" do
    get :access_denied
    flash[:notice].should eql("Acesso negado. Você precisa estar logado.")
  end
end
