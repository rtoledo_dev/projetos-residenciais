require 'spec_helper'
 
describe UsersController do
  fixtures :all
  
  before(:each) do
    spec_login
  end
  
  def valid_attributes
    {:user => attributes_for_valid_user}
  end
  
  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    User.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(users_url)
    flash[:notice].should_not be_nil
  end
  
  it "edit action should render edit template" do
    get :edit, :id => User.first
    response.should render_template(:edit)
  end
  
  it "update action should render edit template when model is invalid" do
    put :update, {:id => User.first, :user => {:login => nil}}
    response.should render_template(:edit)
  end
  
  it "update action should redirect when model is valid" do
    User.any_instance.stubs(:valid?).returns(true)
    put :update, :id => User.first
    response.should redirect_to(users_url)
  end
  
  it "delete 2 users" do
    user_ids = User.find(:all, :limit => 2).collect{|t| t.id}
    put :destroy_all, :ids => user_ids
    response.should redirect_to(users_url)
    User.count(:id, :conditions => {:id => user_ids}).should eql(0)
  end
end
