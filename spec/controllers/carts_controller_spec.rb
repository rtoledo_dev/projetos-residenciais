require File.dirname(__FILE__) + '/../spec_helper'
 
describe CartsController do
  fixtures :all

  before(:each) do
    spec_login
  end
  
  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end
  
  it "show action should render show template" do
    get :show, :id => Cart.first
    response.should render_template(:show)
  end
  
  it "destroy action should destroy model and redirect to index action" do
    cart = Cart.first
    delete :destroy, :id => cart
    response.should redirect_to(carts_url)
    Cart.exists?(cart.id).should be_false
  end
end
