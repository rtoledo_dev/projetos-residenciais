require 'spec_helper'
 
describe ProductValuesController do
  fixtures :all
  
  before(:each) do
    spec_login
  end
  
  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    ProductValue.any_instance.stubs(:valid?).returns(false)
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    ProductValue.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(product_values_url)
  end
  
  it "edit action should render edit template" do
    get :edit, :id => ProductValue.first
    response.should render_template(:edit)
  end
  
  it "update action should render edit template when model is invalid" do
    ProductValue.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ProductValue.first
    response.should render_template(:edit)
  end
  
  it "update action should redirect when model is valid" do
    ProductValue.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ProductValue.first
    response.should redirect_to(product_values_url)
  end
end
