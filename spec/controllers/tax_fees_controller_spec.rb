require File.dirname(__FILE__) + '/../spec_helper'
 
describe TaxFeesController do
  fixtures :all

  before(:each) do
    spec_login
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    TaxFee.any_instance.stubs(:valid?).returns(false)
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    TaxFee.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(new_tax_fee_url)
  end
  
  it "edit action should render edit template" do
    get :edit, :id => TaxFee.first
    response.should render_template(:new)
  end
  
  it "update action should render edit template when model is invalid" do
    TaxFee.any_instance.stubs(:valid?).returns(false)
    put :update, :id => TaxFee.first
    response.should render_template(:new)
  end
  
  it "update action should redirect when model is valid" do
    TaxFee.any_instance.stubs(:valid?).returns(true)
    put :update, :id => TaxFee.first
    response.should redirect_to(new_tax_fee_path)
  end
end
