require File.dirname(__FILE__) + '/../spec_helper'
 
describe NewsController do
  fixtures :all
  integrate_views
  
  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end
  
  it "show action should render show template" do
    get :show, :id => News.first
    response.should render_template(:show)
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    News.any_instance.stubs(:valid?).returns(false)
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    News.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(news_url(assigns[:news]))
  end
  
  it "edit action should render edit template" do
    get :edit, :id => News.first
    response.should render_template(:edit)
  end
  
  it "update action should render edit template when model is invalid" do
    News.any_instance.stubs(:valid?).returns(false)
    put :update, :id => News.first
    response.should render_template(:edit)
  end
  
  it "update action should redirect when model is valid" do
    News.any_instance.stubs(:valid?).returns(true)
    put :update, :id => News.first
    response.should redirect_to(news_url(assigns[:news]))
  end
  
  it "destroy action should destroy model and redirect to index action" do
    news = News.first
    delete :destroy, :id => news
    response.should redirect_to(news_url)
    News.exists?(news.id).should be_false
  end
end
