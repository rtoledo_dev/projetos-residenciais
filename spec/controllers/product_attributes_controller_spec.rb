require 'spec_helper'
 
describe ProductAttributesController do
  fixtures :all
  
  before(:each) do
    spec_login
  end
  
  it "index action should render index template" do
    get :index
    response.should render_template(:index)
  end
  
  it "new action should render new template" do
    get :new
    response.should render_template(:new)
  end
  
  it "create action should render new template when model is invalid" do
    ProductAttribute.any_instance.stubs(:valid?).returns(false)
    post :create
    response.should render_template(:new)
  end
  
  it "create action should redirect when model is valid" do
    ProductAttribute.any_instance.stubs(:valid?).returns(true)
    post :create
    response.should redirect_to(product_attributes_url)
  end
  
  it "edit action should render edit template" do
    get :edit, :id => ProductAttribute.first
    response.should render_template(:edit)
  end
  
  it "update action should render edit template when model is invalid" do
    ProductAttribute.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ProductAttribute.first
    response.should render_template(:edit)
  end
  
  it "update action should redirect when model is valid" do
    ProductAttribute.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ProductAttribute.first
    response.should redirect_to(product_attributes_url)
  end
  
  it "destroy action should destroy model and redirect to index action" do
    product_attribute = ProductAttribute.first
    delete :destroy, :id => product_attribute
    response.should redirect_to(product_attributes_url)
    ProductAttribute.exists?(product_attribute.id).should be_false
  end
end
