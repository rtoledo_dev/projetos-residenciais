["Area total","Area de frente", "Area de fundo", "Quantidade de quartos", "Quantidade de suites",
  "Garagem","Varanda","Terraço","Quintal","Jardim", "Cozinha", "Sala", "Banheiros"].each do |t|
  Attribute.find_or_create_by_name t
end

["Sobrados","1 Quarto", "2 Quartos", "3 Quartos", "4 Quartos",
  "até 50m2","51 a 100m2","101 a 150m2","151 a 200m2","201 a 300m2", 
  "Churrasqueiras", "Piscinas", "Acima de 300m2", "Malocas", "Banheiros"].each do |t|
  Category.find_or_create_by_name t
end

["Comum", "Completo"].each do |t|
  puts ProductType.find_or_create_by_name(t)
end


u = User.find_by_login("administrador")
if u.nil?
  u = User.new :login => "administrador", 
  :name => "Administrador",
  :password => "123456", :password_confirmation => "123456",
  :email => "agc.rodrigo@gmail.com",
  :cpf => "067.013.756-18"
  u.save
  u.has_role! "Admin"
end