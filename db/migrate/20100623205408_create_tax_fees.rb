class CreateTaxFees < ActiveRecord::Migration
  def self.up
    create_table :tax_fees do |t|
      t.float :price
      t.timestamps
    end
  end
  
  def self.down
    drop_table :tax_fees
  end
end
