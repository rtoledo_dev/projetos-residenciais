class AddPrefixInCart < ActiveRecord::Migration
  def self.up
    add_column :carts, :phone_prefix, :string, :size => 4
  end

  def self.down
    remove_column :carts, :phone_prefix
  end
end