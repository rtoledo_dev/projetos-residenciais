class AddLeaderPriceInProductValues < ActiveRecord::Migration
  def self.up
    add_column :product_values, :leader_price, :boolean, :default => false
  end

  def self.down
    remove_column :product_values, :leader_price
    
  end
end