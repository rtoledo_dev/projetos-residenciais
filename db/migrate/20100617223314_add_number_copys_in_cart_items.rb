class AddNumberCopysInCartItems < ActiveRecord::Migration
  def self.up
    add_column :cart_items, :number_copys, :integer, :default => 0
  end

  def self.down
    remove_column :cart_items, :number_copys
  end
end