class CreateCosts < ActiveRecord::Migration
  def self.up
    create_table :costs do |t|
      t.string :name
      t.timestamps
    end
  end
  
  def self.down
    drop_table :costs
  end
end
