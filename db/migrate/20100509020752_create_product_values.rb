class CreateProductValues < ActiveRecord::Migration
  def self.up
    create_table :product_values do |t|
      t.references :product
      t.references :product_type
      t.string :description
      t.timestamps
    end
  end
  
  def self.down
    drop_table :product_values
  end
end
