class ProfilesRoles < ActiveRecord::Migration
  def self.up
    create_table :profiles_roles, :id => false, :force => true do |t|
      t.references :profile
      t.references :role
      t.timestamps
    end
  end

  def self.down
    drop_table :profiles_roles
  end
end
