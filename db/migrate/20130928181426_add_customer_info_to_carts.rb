class AddCustomerInfoToCarts < ActiveRecord::Migration
  def self.up
    add_column :carts, :customer_cep, :string
    add_column :carts, :customer_address, :string
    add_column :carts, :customer_address_number, :string
    add_column :carts, :customer_address_complement, :string
    add_column :carts, :customer_address_neighborhood, :string
    add_column :carts, :customer_address_city, :string
    add_column :carts, :customer_address_state, :string
  end

  def self.down
    remove_column :carts, :customer_cep
    remove_column :carts, :customer_address
    remove_column :carts, :customer_address_number
    remove_column :carts, :customer_address_complement
    remove_column :carts, :customer_address_neighborhood
    remove_column :carts, :customer_address_city
    remove_column :carts, :customer_address_state
  end
end
