class CreateCopyFees < ActiveRecord::Migration
  def self.up
    create_table :copy_fees do |t|
      t.float :price
      t.timestamps
    end
  end
  
  def self.down
    drop_table :copy_fees
  end
end
