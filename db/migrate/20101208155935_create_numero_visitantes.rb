class CreateNumeroVisitantes < ActiveRecord::Migration
  def self.up
    create_table :numero_visitantes do |t|
      t.string :ip

      t.timestamps
    end
  end

  def self.down
    drop_table :numero_visitantes
  end
end
