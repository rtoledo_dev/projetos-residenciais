class CreateBuildCosts < ActiveRecord::Migration
  def self.up
    create_table :build_costs do |t|
      t.references :product
      t.references :cost
      t.float :price
      t.string :state
      t.timestamps
    end
  end
  
  def self.down
    drop_table :build_costs
  end
end
