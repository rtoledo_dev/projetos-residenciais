class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.string :name
      t.float :price
      t.float :m2 # metros quadrados
      
      t.string :mhouse # tamanho da casa
      t.string :msuggestion # tamanho sugerido do terreno
      
      t.boolean :special_ad # anuncio em twitter, orkut e outros
      t.text :description
      t.boolean :is_active
      t.timestamps
    end
  end
  
  def self.down
    drop_table :products
  end
end
