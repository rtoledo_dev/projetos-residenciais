class ChangeCart < ActiveRecord::Migration
  def self.up
    add_column :carts, :customer, :string
    add_column :carts, :email, :string
    add_column :carts, :phone, :string
  end

  def self.down
    remove_column :carts, :phone
    remove_column :carts, :email
    remove_column :carts, :customer
  end
end