class AddLocationsInProjectModification < ActiveRecord::Migration
  def self.up
    add_column :project_modifications, :latitude, :string
    add_column :project_modifications, :longitude, :string
    add_column :project_modifications, :neighborhood, :string
    add_column :project_modifications, :house_number, :string, :limit => 10
  end

  def self.down
    remove_column :project_modifications, :house_number
    remove_column :project_modifications, :neighborhood
    remove_column :project_modifications, :longitude
    remove_column :project_modifications, :latitude
  end
end