class CreatePhotos < ActiveRecord::Migration
  def self.up
    create_table :photos do |t|
      t.references :product
      t.string :photo_file_name
      t.integer :photo_file_size
      t.datetime :photo_updated_at
      t.string :photo_content_type
      t.timestamps
    end
  end
  
  def self.down
    drop_table :photos
  end
end
