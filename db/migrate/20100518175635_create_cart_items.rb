class CreateCartItems < ActiveRecord::Migration
  def self.up
    create_table :cart_items do |t|
      t.references :cart
      t.references :product
      t.references :product_value
      t.float :price
      t.float :amount
      t.integer :quantity, :default => 0
      t.timestamps
    end
  end
  
  def self.down
    drop_table :cart_items
  end
end
