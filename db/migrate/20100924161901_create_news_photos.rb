class CreateNewsPhotos < ActiveRecord::Migration
  def self.up
    create_table :news_photos do |t|
      t.string :photo_file_name
      t.integer :photo_file_size
      t.string :photo_content_type
      t.datetime :photo_updated_at
      t.references :news
      t.timestamps
    end
  end
  
  def self.down
    drop_table :news_photos
  end
end
