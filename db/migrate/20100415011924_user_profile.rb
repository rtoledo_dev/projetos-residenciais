class UserProfile < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.references :profile
    end
  end

  def self.down
    remove_column :users, :profile_id
  end
end
