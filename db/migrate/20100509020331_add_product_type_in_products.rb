class AddProductTypeInProducts < ActiveRecord::Migration
  def self.up
    change_table :products do |t|
      t.references :product_type
    end
  end

  def self.down
    remove_column :products, :product_type_id
  end
end
