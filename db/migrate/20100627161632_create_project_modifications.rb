class CreateProjectModifications < ActiveRecord::Migration
  def self.up
    create_table :project_modifications do |t|
      t.string :type_modification
      t.references :product
      t.string :name
      t.string :email
      t.string :phone, :limit => 30
      t.string :address
      t.string :city
      t.string :cep, :limit => 10
      t.string :province
      t.float :right_side
      t.float :left_side
      t.float :front_side
      t.float :back_side
      t.string :orientation, :limit => 10
      t.text :climatic
      t.boolean :rains
      t.string :terrain_type, :limit => 10
      t.boolean :have_neighborhood
      t.boolean :have_buildings
      t.string :terrain_localization, :limit => 30
      t.integer :quantity_people, :limit => 4
      t.boolean :have_childrens
      t.string :age_childrens, :limit => 20
      t.integer :quantity_cars, :limit => 4
      t.boolean :aged
      t.boolean :room_floor
      t.string :activity
      t.string :have_visits
      t.string :lunch_way, :limit => 30
      t.boolean :tv_together
      t.boolean :have_study_place
      t.boolean :have_pc_rooms
      t.boolean :floor_room
      t.boolean :have_childrens_rooms
      t.boolean :floor_for_others
      t.text :observations
      t.text :modifications_in_rooms
      
      t.string :localization_file_name
      t.integer :localization_file_size
      t.datetime :localization_updated_at
      t.string :localization_content_type
      
      t.string :picture_file_name
      t.integer :picture_file_size
      t.datetime :picture_updated_at
      t.string :picture_content_type
      
      t.timestamps
    end
  end
  
  def self.down
    drop_table :project_modifications
  end
end
