#PROJECTS = %w(brnumeros brdinheiro brcep brdata brhelper brstring brcpfcnpj)
PROJECTS = %w(brcpfcnpj brcep)

PROJECTS.each do |project|
  require "#{File.dirname(__FILE__)}/#{project}/rails/init"
end
