ActionController::Routing::Routes.draw do |map|
  map.resources :comments, :member => {:publish => :get}

  map.resources :news_photos
  
  

  map.resources :news

  map.resources :build_costs

  map.resources :costs
  
  
  map.resources :perguntas_frequentes, :only => :index
  map.resources :how_tos, :collection => {:delete_all => :put}

  map.resources :project_modifications

  map.resources :tax_fees

  map.resources :copy_fees

  map.resources :payments

  map.resources :cart_items

  map.resources :carts

  map.resources :product_values

  map.resources :product_types

  map.resources :product_attributes

  map.resources :attributes

  map.resources :photos

  map.resources :photo3ds

  map.resources :products
  map.resources :projetos, :collection => {:buscar_custos => :get}, :member => {:salvar_comentario => :post}

  map.resources :categories
  map.resources :categorias, :only => [:index, :show]

  map.resources :profiles

  map.resources :user_sessions, :collection => {:denied => :get}

  map.resources :users, :collection => {:delete_all => :put}
  
  map.resources :buscas, :only => :index
  
  map.root :controller => "home"
  map.admin_root "admin_root", :controller => "dashboard", :action => "index"
  map.login "login", :controller => "user_sessions", :action => "new"
  map.logout "logout", :controller => "user_sessions", :action => "destroy"
  map.denied "denied", :controller => "user_sessions", :action => "denied"
  map.informacoes "informacoes", :controller => "informacoes"
  map.ajuda "ajuda", :controller => "ajuda"
  map.modificar_projeto "modificar_projeto", :controller => "modificar_projeto", :action => "index"
  map.crie_seu_projeto "criar_projeto", :controller => "criar_projeto", :action => "index"
  map.mapa_do_site "mapa_do_site", :controller => "mapa_do_site" 
  map.diversos "diversos", :controller => "diversos"
  map.contato "contato", :controller => "contato"
  map.home "home", :controller => "home"
  map.noticias "noticias", :controller => "noticias", :action => "index"
  map.noticia "noticias/:id", :controller => "noticias", :action => "show"
  
  
  
  
  
#  map.connect "carrinho/adicionar/:product_value_id", :controller => "carrinho", :action => :adicionar
  map.connect "carrinho/atualizar/:product_value_id/:quantity", :controller => "carrinho", :action => :atualizar
  
  

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
